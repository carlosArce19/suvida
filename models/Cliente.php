<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cliente
 *
 * @author lidis
 */
class Cliente extends Model{
    
    protected static $table = "Cliente";
    private $id;
    private $nombre;
    private $apellido;
    private $fecha_ingreso;
    private $valor_seguro;
    private $tipo_cliente;
    
    function __construct($id, $nombre, $apellido, $fecha_ingreso, $valor_seguro, $tipo_cliente) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->fecha_ingreso = $fecha_ingreso;
        $this->valor_seguro = $valor_seguro;
        $this->tipo_cliente = $tipo_cliente;
    }
    
    public function getMyVars() {
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getFecha_ingreso() {
        return $this->fecha_ingreso;
    }

    function getValor_seguro() {
        return $this->valor_seguro;
    }

    function getTipo_cliente() {
        return $this->tipo_cliente;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setFecha_ingreso($fecha_ingreso) {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    function setValor_seguro($valor_seguro) {
        $this->valor_seguro = $valor_seguro;
    }

    function setTipo_cliente($tipo_cliente) {
        $this->tipo_cliente = $tipo_cliente;
    }



}
