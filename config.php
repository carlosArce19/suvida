<?php


define('_DB_USER', 'root');
define('_DB_PASS', '');

define('_SERVICE','http://localhost/SuVida/services/');
define('_URL', 'http://localhost/SuVida/app');
define('_BUSSINES','../bussines/');

//DATABASE GLOBALS
define('_DB_NAME', 'suvida');
define('_DB_TYPE', 'mysql');
define('_DB_HOST', 'localhost');

//FOLDERS GLOBALS
define('MODULE','./views/modules/');
define('LIBS', 'libs/');


define('_MODELS', '../models/');

//SECURITY GLOBALS
define('HASH_KEY', 'SOME');
define('HASH_SECRET', 'SECRET');
define('HASH_PASSWORD_KEY', 'STUFF');
define('SECRET_WORD','HERE');

define('LOCAL_SERVER',true);