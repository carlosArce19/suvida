$(document).ready(function () {
    contenido();
    productos();
});

var contenido = function () {

    $.ajax({
        url: 'http://127.0.0.1/sisa_Shop/app/tienda/traerDatos',
        type: 'POST'
    }).done(function (msg) {
//console.log("los datos se recibieron: "+msg);
        $(".barraH").html(msg);
    });

}

var productos = function () {

    $("body").on("click", ".producto", function (e) {
        e.preventDefault();
        e.stopPropagation();
        var target = e.target;
        target = $(target).attr("href");
        parametros = {
            "id": target
        }

        $.ajax({
            type: "post",
            url: "http://127.0.0.1/sisa_Shop/app/tienda/traerProductos",
            data: parametros
        }).done(function (msg) {
//console.log("los datos se recibieron: "+msg);
            $(".containerProducto").remove();
            $(".workspace").append(msg);
        });

    });


    $("body").on("click", ".imagen", function (e) {
        var imagen = e.target;
        var src = imagen.src;
        var contenedor = $('.contenedorImagen').children('img');
        contenedor[0].src = src;
    });

    $("body").on("click", ".agregarCarro", function (e) {
        e.preventDefault();
        e.stopPropagation();
        var target = e.target;
        var hijos = $(".workspace").children(".containerProducto");
        var div = $(target).attr("pertenece");
        target = $(target).attr("producto");
        div = $(hijos[div]).children(".cantidadCarro").val();
        //div = $(div).parent(".cantidadCarro");
        parametros = {
            "cantidad": div,
            "producto": target
        };

        $.ajax({
            type: "post",
            url: "http://127.0.0.1/sisa_Shop/app/tienda/agregarCarrito",
            data: parametros
        }).done(function (msg) {
//console.log("los datos se recibieron: "+msg);
            alert(msg)
        });

    });

    
}
