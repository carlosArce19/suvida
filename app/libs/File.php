<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File
 *
 * @author Hardcore
 */
class File {
    
    public static function upload($carpeta, $nombreInput, $nombre = false, $checkFormat = false, $sizeLimit = false) {

        $target_dir = $carpeta; //carpeta del servidor donde guardamos las imagenes
        $extension = explode(".", basename($_FILES[$nombreInput]["name"]));
        $extension = $extension[count($extension) - 1];
        $target_file = ($nombre) ? $target_dir . $nombre . "." . $extension : $target_dir . basename($_FILES[$nombreInput]["name"]); //ubicamos el fantasma
        //echo 'target file:'.$target_file;
        $uploadOk = 1; //estado de la subida
        $fileType = pathinfo($target_file, PATHINFO_EXTENSION); //extraemos el tipo de fantasma
        //
        // 
        // Check file size
        if ($sizeLimit > 0) {
            if ($_FILES[$nombreInput]["size"] > $sizeLimit) {
                $uploadOk = 0;
            }
        }
        // Allow certain file formats
        if ($checkFormat) {
            //Hacer la verificacion de formatos
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 1) {
            //si el fantasma no fue imagen o si el peso es excesivo o si no cumple con el formato, entonces no lo atrape, pero si cumple todos los filtros, atrapelo
            if (move_uploaded_file($_FILES[$nombreInput]["tmp_name"], $target_file)) {//si se completa la atrapada del fantasma (ubicandolo en la direcciòn deseada)
                return $target_file;
            } else {
                return false;
            }
        }
    }
    
}
