<?php

class Factura {

    public function generar($num, $data) {


//Recibir detalles de factura
        $id_factura = $num;

//variable que guarda el nombre del archivo PDF
        $archivo = "factura-$id_factura.pdf";
        print_r($data["nombre"]);

//Llamada al script fpdf
        require('fpdf.php');

        $totalPaga = $data["precio_unidad"] * $data["cantidad"];
        $archivo_de_salida = $archivo;

        $pdf = new FPDF();  //crea el objeto
        $pdf->AddPage();  //a�adimos una p�gina. Origen coordenadas, esquina superior izquierda, posici�n por defeto a 1 cm de los bordes.
//logo de la tienda
//$pdf->Image('../empresa.jpg' , 0 ,0, 40 , 40,'JPG', 'http://php-estudios.blogspot.com');
// Encabezado de la factura
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(190, 10, "FACTURA", 0, 2, "C");
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(190, 5, "Numero de factura: $id_factura" . "\n", 0, "C", false);
        $pdf->Ln(2);

// Datos del cliente
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(20, 50);
        $pdf->Cell(190, 10, "Datos del cliente", 0, 2, "J");
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(
                190, //posici�n X
                5, //posicion Y
                "Nombre: " . $data["nombre"] . "\n" .
                "Apellidos: " . $data["apellido"] . "\n", 0, // bordes 0 = no | 1 = si
                "J", // texto justificado
                false);


// Datos del cliente
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(140, 40);
        $pdf->Cell(190, 10, "Datos del producto", 0, 2, "J");
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(
                190, //posici�n X
                5, //posicion Y
                "Nombre: " . $data["nombreProducto"] . "\n" .
                "Precio Unidad: " . $data["precio_unidad"] . "\n" .
                "Cantidad: " . $data["cantidad"], 0, // bordes 0 = no | 1 = si
                "J", // texto justificado
                false);

// Datos del cliente
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(90, 110);
        $pdf->Cell(190, 10, "TOTAL:", 0, 2, "J");
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(
                190, //posici�n X
                5, //posicion Y
                "TOTAL: " . $totalPaga . "\n", 0, // bordes 0 = no | 1 = si
                "J", // texto justificado
                false);


        $pdf->Output($archivo_de_salida); //cierra el objeto pdf
//Creacion de las cabeceras que generar�n el archivo pdf
        header("Content-Type: application/download");
        header("Content-Disposition: attachment; filename=$archivo");
        header("Content-Length: " . filesize("$archivo"));
        $fp = fopen($archivo, "r");
        fpassthru($fp);
        fclose($fp);

//Eliminaci�n del archivo en el servidor
        $mailer = new AttachMailer("juanbetan93@gmail.com", $data["email"], "Email Sender", "hello <b>esta es tu factura</b>");
        $mailer->attachFile($archivo);
        $mailer->send() ? "Success" : "Ooops, an Error occurred!";
//unlink($archivo);
    }

}
