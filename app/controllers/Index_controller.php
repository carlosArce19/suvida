<?php

  class Index_controller extends Controller{
    
    public function __construct() {
        parent::__construct();
        
    }
    
    
    function index(){
        $cliPref=  Cliente::searchOrder("tipo_cliente='p'","fecha_ingreso");
        $this->view->clientesPreferenciales = $cliPref;
        
         $cliBasico=  Cliente::searchOrder("tipo_cliente='b'","fecha_ingreso");
        $this->view->clientesBasico = $cliBasico;
        
        $this->view->render($this,"index");
    }
  }