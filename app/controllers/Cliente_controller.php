<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cliente_controller
 *
 * @author lidis
 */
class Cliente_controller extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function signUp(){
        
        
        if (isset($_POST["id"]) && isset($_POST["nombre"]) && isset($_POST["apellido"]) &&
                isset($_POST["fecha_ingreso"]) && isset($_POST["valor_seguro"])) {

            $id = $_POST["id"];
            $nombre = $_POST["nombre"];
            $apellido = $_POST["apellido"];
            $fecha_ingreso = $_POST["fecha_ingreso"];
            $valor_seguro = $_POST["valor_seguro"];

            if (trim($id) != "" && trim($nombre) != "" && trim($apellido) != "" &&
                    trim($fecha_ingreso) != "" && trim($valor_seguro) != "") {
                
                $cli= Cliente::getById($id);
                
                if ($cli == null) {
                    if (is_numeric($valor_seguro) && $valor_seguro>0) {
                        $tipo_cliente = $valor_seguro > 100000000 ? "p" : "b";
                        $cliente = new Cliente($id, $nombre, $apellido, $fecha_ingreso, $valor_seguro, $tipo_cliente);
                        $respuesta = $cliente->create();
                        if ($respuesta["error"] == 0) {
                            echo '6';
                        } else {
                            echo '5';
                        }
                    } else {
                        echo '4';
                    }
                } else {
                    echo '3';
                }
            } else {
                echo '2';
            }
        } else {
            echo '1';
        }
    }
    
    public function buscar() {
        if (isset($_POST["id"])) {

            $id = $_POST["id"];
            if (trim($id) != "") {
                $cliente = Cliente::getById($id);
                if ($cliente != NULL) {
                    
                    print_r($cliente);
                } else {
                    echo '3';
                }
            } else {
                echo '2';
            }
        } else {
            echo '1';
        }
    }

}
