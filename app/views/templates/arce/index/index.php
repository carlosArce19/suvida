<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SisasShop</title>
        <link rel="stylesheet" href="<?php echo _URL; ?>/public/styles/stylesheet.css">
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    </head>
    <body>

        <?php if (!Session::exist()) { ?>
            <div id="formWrapper hidden">

                <div class="formWrapper">
                    <div class="formTitle">Entrar</div>
                    <form id="signInForm" action="<?php echo _URL; ?>/Usuario/signIn"  name="signIn" method="post">
                        <input class="singInValue" name="email" type="text" placeholder="Email" required/>
                        <input class="singInValue" name="password" type="password" placeholder="Password" required/>
                        <input class="singInValue" id="signInBtn" name="signInBtn" type="submit" value="Entrar" required/>
                        <div class="smallText">
                            <span>¿No estas registrado? <div class="button" id="signUpButton">Registrate Aquí</div></span>

                        </div>
                    </form>
                </div>

                <div class="formWrapper">
                    <div class="formTitle">Registros</div>
                    <form id="signUpForm" action="<?php echo _URL; ?>/Usuario/signUp"  name="signUp" method="post" enctype="multipart/form-data">

                        <input name="nombre" type="text" placeholder="Nombre" required/>
                        <input name="apellido" type="text" placeholder="Apellido" required/>
                        <input name="password" type="password" placeholder="Password" required/>
                        <input name="password2" type="password" placeholder="Password Nuevamente" required/>
                        <input name="email" type="text" placeholder="Email" required/>
                        <div>
                            <input id="nomTienda" name="nomTienda" type="text" placeholder="Nombre Tienda" />
                        </div>



                        <input id="signUpBtn" name="signUpBtn" type="submit" value="Registrarme" required/>
                        <div class="smallText">
                            <span>¿Si estas registrado? <div class="button" id="signInButton">Volver</div></span>
                        </div>
                    </form>
                </div>

            </div>
            <script>
                $(function () {

                    $('#signUpButton').click(function () {

                        $("form[name=signIn]").parent().hide();
                        $("form[name=signUp]").parent().fadeToggle();
                    });

                    $('#signInButton').click(function () {
                        $("form[name=signUp]").parent().hide();
                        $("form[name=signIn]").parent().fadeToggle();
                    });

                    $('#signInBtn').click(function (e) {
                        e.preventDefault();
                        signIn();
                    });
                });

                


                function signIn() {

                    var email = $('form[name=signIn] input[name=email]')[0].value;
                    var password = $('form[name=signIn] input[name=password]')[0].value;

                    $.ajax({
                        type: "POST",
                        url: "<?php echo _URL; ?>/Usuario/signIn",
                        data: {email: email, password: password}
                    }).done(function (response) {
                       
                        if (response == 1) {
                            location.reload();
                        } else {
                            alert("Usuario o Password Incorrectos");
                        }
                    });
                }
            </script>
        <?php
        } else {
            if ($_SESSION['ROL'] == 1) {
                header("Location:" . _URL . "/panel");
            } else if ($_SESSION['ROL'] == 2) {
                //header("Location:".URL."panelVendedor/"); 
            }else if($_SESSION['ROL'] == 4) {
                    header("Location:" . _URL . "/tienda");
            } 
            else {
                //header("Location:".URL."panelComprador/");
            }

            //Session::destroy();
        }
        ?>
    </body>
</html>