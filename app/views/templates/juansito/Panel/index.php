<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title>Panel Admin Juansito</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="./libs/app.js"></script>
        <link rel="stylesheet" href="./views/templates/juansito/Panel/css/app.css">
    </head>

    <body>
        <div class="panelIzq"> 
            <div class="barraH">
                <div class="subir"><i class="ion-android-archive"></i> Crear Categoria</div>
                <div class ="eliminar"><i class="ion-android-delete"></i>Producto</div>
                <div class="crear-lista"><i class="ion-android-list"></i> Crear lista</div>
                <div class="editar-lista"><i class="ion-compose"></i> Editar lista</div>
                <div class="eliminar-lista"><i class="ion-android-delete"></i> Eliminar Lista</div>
                <div class="editar-perfil"><i class="ion-edit"></i> Editar Perfil</div>
            </div>
        </div>
        <div class="workspace">

            <form action="<?php echo _URL; ?>/panel/categoria" method="POST">
                Nombre categoria <input type="text" name="nombre"><br>
                Que opciones tendra su categoria:<br>
                <input type="checkbox" name="nombreProducto" value="nombreProducto," />nombre producto
                <input type="checkbox" name="titulo" value="titulo," />titulo
                <input type="checkbox" name="descripcion" value="descripcion," />descripcion
                <input type="checkbox" name="imagenes" value="imagenes," />imagenes<br />
                <input type="checkbox" name="fechaVencimiento" value="fechaVencimiento," />fechaVencimiento<br />
                <input type="checkbox" name="keywords" value="keywords," />keywords
                <input type="checkbox" name="talla" value="talla," />talla
                <input type="checkbox" name="color" value="color," />color<br />
                <input type="checkbox" name="editorial" value="editorial," />editorial                                                                                                                                          
                <input type="checkbox" name="fabricante" value="fabricante," />fabricante
                <input type="checkbox" name="autor" value="autor," />autor<br />
                <input type="checkbox" name="peso" value="peso," />peso<br />  
                <input type="submit" value="Submit">
            </form>



            <form action="<?php echo _URL; ?>/panel/subcategoria" method="POST">
                Nombre subcategoria <input type="text" name="nombre"><br>
                <select name="id_padre">
                    <option value="">Seleccione Categoria</option>
                    <?php foreach ($this->categorias as $categoria) : ?>
                        <option value="<?php echo $categoria['id']; ?>"><?php echo $categoria['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" value="Submit">
            </form>    

            <form id="cambiar" action="<?php echo _URL; ?>/panel/crearProducto" method="POST" enctype="multipart/form-data">
                <select name="categoria" style="display:block">
                    <option value="0">Seleccione a que categoria pertenece el producto</option>
                    <?php foreach ($this->categorias as $categoria) : ?>
                        <option value="<?php echo $categoria['id']; ?>"><?php echo $categoria['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>

            </form>  

            <form action="<?php echo _URL; ?>/panel/template" method="POST" enctype="multipart/form-data">
                Seleciones Template
                <input type="file" name="file">
                <input id="varios" class="button" type="submit">

            </form>  
            
            <form action="<?php echo _URL; ?>/panel/cambiarTemplate" method="POST">
                <select name="id_template">
                    <option value="-1">Seleccione el template</option>
                    <?php foreach ($this->template as $tem) : ?>
                        <option value="<?php echo $tem['id']; ?>"><?php echo $tem['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
                
                 <select  name="id_tienda">
                    <option value="-1">Seleccione la tienda</option>
                    <?php foreach ($this->tienda as $tien) : ?>
                        <option value="<?php echo $tien['id']; ?>"><?php echo $tien['titulo']; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" value="Submit">
            </form> 


            <form action="<?php echo _URL; ?>/panel/crearBodega" method="POST">
                Nombre Bodega <input type="text" name="nombre"><br>
                <select name="id_tienda">
                    <option value="">Seleccione la tienda</option>
                    <?php foreach ($this->tienda as $tien) : ?>
                        <option value="<?php echo $tien['id']; ?>"><?php echo $tien['titulo']; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" value="Submit">
            </form>       

            <form action="<?php echo _URL; ?>/panel/añadirBodega" method="POST">
                Añadir productos a bodega<br>
                <select name="id_producto">
                    <option value="">Seleccione el producto</option>
                    <?php foreach ($this->pro as $pro) : ?>
                        <option value="<?php echo $pro['id']; ?>"><?php echo $pro['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>

                <select name="id_bodega">
                    <option value="">Seleccione la bodega</option>
                    <?php foreach ($this->bode as $bode) : ?>
                        <option value="<?php echo $bode['id']; ?>"><?php echo $bode['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
                Cantidad <input type="text" name="cantidad"><br>
                <input type="submit" value="Submit">
            </form> 

            <form id="enviarCorreos" name="enviarCorreos" action="<?php echo _URL; ?>/panel/enviarCorreos" method="POST">
                <input class="enviarCorreosValue" name="asunto" type="text" placeholder="asunto del correo" required/>
                <textarea id="msgText"name="mensaje" rows="10" cols="50" placeholder="Mensaje"></textarea>
                <select id="idTiendaSelcted2" name="id_tienda">
                    <option value="-1">Seleccione la tienda</option>
                    <?php foreach ($this->tienda as $tien) : ?>
                        <option value="<?php echo $tien['id']; ?>"><?php echo $tien['titulo']; ?></option>
                    <?php endforeach; ?>
                </select>
                <select id="idSelectedRol" name="id_rol">
                    <option value="-1">Seleccione a destinatarios</option>
                    <?php foreach ($this->rol as $rol) : ?>
                        <option value="<?php echo $rol['id']; ?>"><?php echo $rol['nombre']; ?></option>
                    <?php endforeach; ?>
                    <option value="-2">Todos</option>
                </select>
                <input id="vistPrevia" class="vistPreviaBtn" type="button" value="Vista Previa" onclick="vistaPrevia();"/>
                <input class="enviarCorreosValue" id="enviarCorreosBtn" name="enviarCorreosBtn" type="submit" value="Enviar" required/>

            </form>
            
            <form id="cambiarProducto" action="<?php echo _URL; ?>/panel/editarProducto" method="POST" enctype="multipart/form-data">
                Elija producto a editar<br>
                <select name="productoEditar">
                    <option value="">Seleccione el producto</option>
                    <?php foreach ($this->pro as $pro) : ?>
                        <option value="<?php echo $pro['id']; ?>"><?php echo $pro['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
                
               

                
            </form> 

        </div>
        <script>

            $(function () {

                $('#enviarCorreosBtn').click(function (e) {
                    e.preventDefault();
                    enviarCorreos();
                });
            });
            function vistaPrevia() {


                var tienda = $("#idTiendaSelcted2 option:selected").text();
                var mensaje = $("#msgText").val();
                alert("Buenas [Nombre Usuario] "+"de parte de " + tienda + "\n \n" + mensaje);
            }

            function enviarCorreos() {

                var asunto = $('form[name=enviarCorreos] input[name=asunto]')[0].value;
                var mensaje=$("#msgText").val();
                var tienda = $("#idTiendaSelcted2 option:selected").text();
                var destinatarios=$("#idSelectedRol").val();

                $.ajax({
                    type: "POST",
                    url: "<?php echo _URL; ?>/panel/enviarCorreos",
                    data: {asunto: asunto, mensaje: mensaje, destinatario:destinatarios,tienda:tienda}
                }).done(function (response) {

                    if (response == 1) {
                        alert("Mensaje Enviado Exitosamente");
                    } else {
                        alert(response);
                    }
                });
            }

        </script>

    </body>

</html>