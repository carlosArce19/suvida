<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SisasShop</title>
        <link rel="stylesheet" href="<?php echo _URL; ?>/public/styles/stylesheet.css">
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    </head>
    <body>

            <div id="formWrapper hidden">
                <div class="formWrapper">
                    <div class="formTitle">Registros</div>
                    <form id="signUpForm" action="<?php echo _URL; ?>/Cliente/signUp"  name="signUp" method="post" enctype="multipart/form-data">

                        <input name="id" type="text" placeholder="Codigo de Registro" required/>
                        <input name="nombre" type="text" placeholder="Nombre" required/>
                        <input name="apellido" type="text" placeholder="Apellido" required/>
                        <input name="fecha_ingreso" type="date" placeholder="Fecha Ingeso" required/>
                        <input name="valor_seguro" type="text" placeholder="Valor del Seguro" required/>
                        <div>
                            <input id="nomTienda" name="nomTienda" type="text" placeholder="Nombre Tienda" />
                        </div>




                        <input id="signUpBtn" name="signUpBtn" type="submit" value="Registrarme" required/>
                    </form>
                </div>

            </div>
        
        <table>
            <tr>
                <th>Codigo Registro</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Fecha Ingreso</th>
                <th>Valor Ingreso</th>
                <th>Tipo Usuario</th>
                
            </tr>
            
           
                <?php for ($index = 0; $index < count($this->clientesPreferenciales); $index++) {?>
            <tr> 
                <td><?php echo $this->clientesPreferenciales[$index]["id"]?></td>
                <td><?php echo $this->clientesPreferenciales[$index]["nombre"]?></td>
                <td><?php echo $this->clientesPreferenciales[$index]["apellido"]?></td>
                <td><?php echo $this->clientesPreferenciales[$index]["fecha_ingreso"]?></td>
                <td><?php echo $this->clientesPreferenciales[$index]["valor_seguro"]?></td>
                <td><?php echo $this->clientesPreferenciales[$index]["tipo_cliente"]?></td>
            </tr>
                <?php }?>
            
            
        </table>
        <table>
            <tr>
                <th>Codigo Registro</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Fecha Ingreso</th>
                <th>Valor Ingreso</th>
                <th>Tipo Usuario</th>
                
            </tr>
            
            
                <?php for ($index = 0; $index < count($this->clientesBasico); $index++) {?>
            <tr> 
                <td><?php echo $this->clientesBasico[$index]["id"]?></td>
                <td><?php echo $this->clientesBasico[$index]["nombre"]?></td>
                <td><?php echo $this->clientesBasico[$index]["apellido"]?></td>
                <td><?php echo $this->clientesBasico[$index]["fecha_ingreso"]?></td>
                <td><?php echo $this->clientesBasico[$index]["valor_seguro"]?></td>
                <td><?php echo $this->clientesBasico[$index]["tipo_cliente"]?></td>
            </tr>
                <?php }?>
            
            
        </table>
        
        <div id="formWrapper hidden">
                <div class="formWrapper">
                    <div class="formTitle">Busqueda</div>
                    <form id="signUpForm" action="<?php echo _URL; ?>/Cliente/buscar"  name="buscar" method="post" enctype="multipart/form-data">

                        <input name="id" type="text" placeholder="Codigo de Registro" required/>
                        <input id="buscarBtn" name="signUpBtn" type="submit" value="Buscar" required/>
                    </form>
                </div>

            </div>
            <script>
                $(function () {
                    $('#buscarBtn').click(function (e) {
                        
                        e.preventDefault();
                        buscar();
                    });
                    
                    $('#signUpBtn').click(function (e) {
                        
                        e.preventDefault();
                        signUp();
                    });
                });
                
                function buscar(){
                    var id= $('form[name=buscar] input[name=id]')[0].value;
                   

                    $.ajax({
                        type: "POST",
                        url: "<?php echo _URL; ?>/Cliente/buscar",
                        data: {id:id}
                    }).done(function (response) {
                        
                        if (response == 1) {
                            alert("Debe ingresar todos los datos");
                        } else if(response == 2){
                            alert("Debe ingresar el codigo de registro");
                        }else if(response == 3){
                            alert("El cliete con ese Codigo de registro no existe");
                        }else {
                            alert(response);
                        }
                    });
                }

                function signUp() {
                    
                    var id= $('form[name=signUp] input[name=id]')[0].value;
                    var nombre = $('form[name=signUp] input[name=nombre]')[0].value;
                    var apellido = $('form[name=signUp] input[name=apellido]')[0].value;
                    var fecha_ingreso = $('form[name=signUp] input[name=fecha_ingreso]')[0].value;
                    var valor_seguro = $('form[name=signUp] input[name=valor_seguro]')[0].value;

                    $.ajax({
                        type: "POST",
                        url: "<?php echo _URL; ?>/Cliente/signUp",
                        data: {id:id,nombre:nombre,apellido:apellido,fecha_ingreso:fecha_ingreso,valor_seguro:valor_seguro}
                    }).done(function (response) {
                        
                        
                        if (response == 1) {
                            alert("Debe ingresar todos los datos");
                        } else if(response == 2){
                            alert("Debe ingresar todos los datos");
                        }else if(response == 3){
                            alert("El cliete con ese Codigo de registro ya existe");
                        }else if(response == 4){
                            alert("El valor del seguro debe ser numerico, entero y mayor a cero.");
                        }else if(response == 5){
                            alert("Ha ocurrido un error, intentelo de nuevo.");
                        }else if(response == 6){
                            alert("El cliente se a registrado con exito.");
                            location.reload();
                        }
                    });
                }
            </script>
        
    </body>
</html>